import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginWindow {

	private JFrame frame;
	private JTextField textBenutzername;
	private JTextField textPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginWindow window = new LoginWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblBenutzername = new JLabel("Benutzername:");
		lblBenutzername.setBounds(35, 60, 77, 14);
		frame.getContentPane().add(lblBenutzername);
		
		textBenutzername = new JTextField();
		textBenutzername.setBounds(122, 57, 172, 20);
		frame.getContentPane().add(textBenutzername);
		textBenutzername.setColumns(10);
		
		JLabel lblPasswort = new JLabel("Passwort");
		lblPasswort.setBounds(35, 97, 46, 14);
		frame.getContentPane().add(lblPasswort);
		
		textPassword = new JTextField();
		textPassword.setBounds(122, 94, 172, 20);
		frame.getContentPane().add(textPassword);
		textPassword.setColumns(10);
		
		JButton btnNewButton = new JButton("Ok");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				String benutzername = textBenutzername.getText();
				String pwd = textPassword.getText();
				System.out.println(benutzername);
				System.out.println(pwd);
			}
		});
		btnNewButton.setBounds(138, 148, 89, 23);
		frame.getContentPane().add(btnNewButton);
	}
}
