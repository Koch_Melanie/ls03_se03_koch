package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MyGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyGUI frame = new MyGUI(); // Oberfl�che wird erstellt
					frame.setVisible(true); // Oberfl�che wird angezeigt
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300); // Position auf dem Bildschirm und Gr��e
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblHalloWelt = new JLabel("Hallo Welt!");
		lblHalloWelt.setBounds(10, 11, 60, 23);
		contentPane.add(lblHalloWelt); // Label wird dem ContentFame hinzugef�gt

		JButton btnRed = new JButton("Red");
		btnRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Anweisungsblock beim Klick auf den Button "Red"
				buttonRed_clicked();
			}
		});
		btnRed.setBounds(132, 140, 89, 23);
		contentPane.add(btnRed);

		JButton btnbuttonBlue = new JButton("blue");
		btnbuttonBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Anweisungsblock beim Klick auf den Button "Blue"
				buttonBlue_clicked();

			}

		});
		btnbuttonBlue.setBounds(236, 140, 89, 23);
		contentPane.add(btnbuttonBlue);

		
		
		JButton btnYellow = new JButton("Yellow");
		btnYellow.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				buttonYellow_clicked();

			}
		});
		btnYellow.setBounds(10, 140, 89, 23);
		contentPane.add(btnYellow);
	}

	
	
	
	public void buttonRed_clicked() {
		this.contentPane.setBackground(Color.RED); // �ndern der Hintnergrundfarbe
	}

	public void buttonBlue_clicked() {
		this.contentPane.setBackground(Color.BLUE);

	}

	public void buttonYellow_clicked() {

		this.contentPane.setBackground(Color.YELLOW);
	}
}
